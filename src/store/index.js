import { createStore } from 'vuex'

export default createStore({
  state: {
    paises: [],
    paisesFiltrados: []
  },
  mutations: {
    setPaises (state, paises) {
      state.paises = paises
    },
    setPaisesFiltrados (state, paises) {
      state.paisesFiltrados = paises
    }
  },
  actions: {
    async getPaises ({commit}) {
      try {
        const res = await fetch('api.json')
        const data = await res.json()
        commit('setPaises', data)
      } catch (e) {
        console.log(e);
      }
    },
    filtrarRegion({commit, state}, region) {
      const filtro = state.paises.filter(pais => {
        return pais.region.includes(region)
      })

      commit('setPaisesFiltrados', filtro)
    },
    filtroNombre({commit, state}, texto) {
      const textoCliente = texto.toLowerCase()
      const filtro = state.paises.filter(pais => {
        const textoApi = pais.name.toLowerCase()
        if (textoApi.includes(textoCliente)) {
          return pais
        }
      })
      commit('setPaisesFiltrados', filtro)
    }
  },
  getters: {
    paisesPoblacion(state) {
      return state.paisesFiltrados.sort((a, b) => {
        return a.population < b.population ? 1 : -1
      })
    }
  },
  modules: {
  }
})
